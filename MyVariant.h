#pragma once

#include<cstddef>
#include <boost/mpl/vector.hpp>
#include <boost/mpl/at.hpp>
#include <boost/mpl/size.hpp>
#include <boost/mpl/find.hpp>


template<typename Arg0, typename... Args>
struct FindSize
{
  static constexpr std::size_t size = sizeof(Arg0) > FindSize<Args...>::size ? sizeof(Arg0) : FindSize<Args...>::size;
};

template<typename Arg0>
struct FindSize<Arg0>
{
  static constexpr std::size_t size = sizeof(Arg0);
};

template<std::size_t Index, typename FunctionType, typename Arg0, typename... Args>
struct Visitor
{
  static auto Visit(std::size_t index, FunctionType&& function, const std::byte* data) noexcept
  {
    if (index == Index)
    {
      return function(*reinterpret_cast<const Arg0*>(data));
    }
    else
    {
      return Visitor<Index+1, FunctionType, Args...>::Visit(index, std::move(function), data);
    }
  }
};

template<std::size_t Index, typename FunctionType, typename Arg0>
struct Visitor<Index, FunctionType, Arg0>
{
  static auto Visit(std::size_t index, FunctionType&& function, const std::byte* data) noexcept
  {
    if (index == Index)
    {
      return function(*reinterpret_cast<const Arg0*>(data));
    }
    return decltype(function(Arg0()))();
  }
};

template<typename... Args>
class MyVariant
{
  using types = boost::mpl::vector<Args...>;

public:
  MyVariant() noexcept
  {
    *this = typename boost::mpl::at<types, boost::mpl::int_<0>>::type{};
  }

  template<typename T>
  MyVariant(const T& value) noexcept
  {
    *this = value;
  }

  template<typename T>
  void operator=(const T& value) noexcept
  {
    index_ = boost::mpl::find<types, T>::type::pos::value;
    if (index_ < boost::mpl::size<types>::type::value)
    {
      reinterpret_cast<T&>(data_[0]) = value;
    }
  }

  template<typename FunctionType>
  auto Visit(FunctionType&& function) const noexcept
  {
    return Visitor<0, FunctionType, Args...>::Visit(index_, std::move(function), data_);
  }

private:
  std::byte data_[FindSize<Args...>::size];
  std::size_t index_{0};
};
