#include <iostream>
#include <chrono>
#include <variant>
#include <vector>
#include "test.h"

int main()
{
  auto resPoly   = testPoly();
  auto resVari   = testVariant();
  auto resMyVari = testMyVariant();
  auto resLambda = testLambda();
  auto resStatic = testStatic();

  std::cout << "Static:    " << std::get<0>(resStatic) << "   in " << std::get<1>(resStatic) << " ms\n";
  std::cout << "Lambda:    " << std::get<0>(resLambda) << "   in " << std::get<1>(resLambda) << " ms\n";
  std::cout << "MyVariant: " << std::get<0>(resMyVari) << "   in " << std::get<1>(resMyVari) << " ms\n";
  std::cout << "Variant:   " << std::get<0>(resVari)   << "   in " << std::get<1>(resVari)   << " ms\n";
  std::cout << "Virtual:   " << std::get<0>(resPoly)   << "   in " << std::get<1>(resPoly)   << " ms\n";

  return 0;
}

