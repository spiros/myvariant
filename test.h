#pragma once

#include <cstdint>

std::tuple<int, std::uint64_t> testPoly();
std::tuple<int, std::uint64_t> testVariant();
std::tuple<int, std::uint64_t> testMyVariant();
std::tuple<int, std::uint64_t> testLambda();
std::tuple<int, std::uint64_t> testStatic();
