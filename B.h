#pragma once
#include "P.h"

class Bp : public P
{
public:
  int id() const override;
};

class Bv
{
public:
  int id() const noexcept { return 2; }
};
