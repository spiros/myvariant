#include <chrono>
#include <vector>
#include <variant>
#include "MyVariant.h"
#include "A.h"
#include "B.h"
#include "P.h"

std::tuple<int, std::uint64_t> testPoly()
{
  std::vector<P*> objs;
  std::vector<P*> throwaway;
  for (int i = 0; i < 8192; ++i)
  {
    if (i%3 == 0)
    {
      objs.push_back(new Ap());
    }
    else
    {
      objs.push_back(new Bp());
    }
    throwaway.push_back(new Ap());
    throwaway.push_back(new Ap());
    throwaway.push_back(new Ap());
    throwaway.push_back(new Ap());
    throwaway.push_back(new Ap());
    throwaway.push_back(new Ap());
    throwaway.push_back(new Ap());
    throwaway.push_back(new Ap());
    throwaway.push_back(new Ap());
    throwaway.push_back(new Ap());
    throwaway.push_back(new Ap());
    throwaway.push_back(new Ap());
    throwaway.push_back(new Ap());
    throwaway.push_back(new Ap());
    throwaway.push_back(new Ap());
  }

  int res = 0;

  auto start = std::chrono::high_resolution_clock::now();
  for (size_t i = 0; i < 100000; i++)
  {
    for (auto &obj : objs)
    {
      res += obj->id();
    }
  }
  auto end = std::chrono::high_resolution_clock::now();
  std::uint64_t ms = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();

  for (auto obj : objs)
  {
    delete obj;
  }
  for (auto obj : throwaway)
  {
    delete obj;
  }

  return std::make_tuple(res, ms);
}

std::tuple<int, std::uint64_t> testVariant()
{
  std::vector<std::variant<Av, Bv>> objs;
  for (int i = 0; i < 8192; ++i)
  {
    if (i%3 == 0)
    {
      objs.push_back(Av());
    }
    else
    {
      objs.push_back(Bv());
    }
  }

  int res = 0;

  auto start = std::chrono::high_resolution_clock::now();
  for (size_t i = 0; i < 100000; i++)
  {
    for (auto &obj : objs)
    {
      res += std::visit([](const auto &o) {
        return o.id();
      }, obj);
    }
  }

  auto end = std::chrono::high_resolution_clock::now();
  std::uint64_t ms = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();

  return std::make_tuple(res, ms);
}

std::tuple<int, std::uint64_t> testMyVariant()
{
  std::vector<MyVariant<Av, Bv>> objs;
  for (int i = 0; i < 8192; ++i)
  {
    if (i%3 == 0)
    {
      objs.emplace_back(Av());
    }
    else
    {
      objs.emplace_back(Bv());
    }
  }

  int res = 0;

  auto start = std::chrono::high_resolution_clock::now();
  for (size_t i = 0; i < 100000; i++)
  {
    for (auto &obj : objs)
    {
      res += obj.Visit([](const auto &o) {
        return o.id();
      });
    }
  }

  auto end = std::chrono::high_resolution_clock::now();
  std::uint64_t ms = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();

  return std::make_tuple(res, ms);
}

std::tuple<int, std::uint64_t> testLambda()
{
  std::vector<std::function<int()>> objs;
  for (int i = 0; i < 8192; ++i)
  {
    if (i%3 == 0)
    {
      objs.push_back([]{ return 1; });
    }
    else
    {
      objs.push_back([]{ return 2; });
    }
  }

  int res = 0;

  auto start = std::chrono::high_resolution_clock::now();
  for (size_t i = 0; i < 100000; i++)
  {
    for (auto &obj : objs)
    {
      res += obj();
    }
  }

  auto end = std::chrono::high_resolution_clock::now();
  std::uint64_t ms = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();

  return std::make_tuple(res, ms);
}
 
std::tuple<int, std::uint64_t> testStatic()
{
  std::vector<int> objs;
  for (int i = 0; i < 8192; ++i)
  {
    if (i%3 == 0)
    {
      objs.push_back(1);
    }
    else
    {
      objs.push_back(2);
    }
  }

  int res = 0;

  auto start = std::chrono::high_resolution_clock::now();
  for (size_t i = 0; i < 100000; i++)
  {
    for (auto &obj : objs)
    {
      res += obj;
    }
  }

  auto end = std::chrono::high_resolution_clock::now();
  std::uint64_t ms = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();

  return std::make_tuple(res, ms);
}
