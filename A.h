#pragma once
#include "P.h"

class Ap : public P
{
public:
  int id() const override;
};

class Av
{
public:
  int id() const noexcept { return 1; }
};
