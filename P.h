#pragma once

class P
{
public:
  virtual ~P() =default;
  virtual int id() const = 0;
};

